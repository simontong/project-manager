module.exports = function(User) {

    // User.disableRemoteMethod('create', true);
    // User.disableRemoteMethod('upsert', true);
    User.disableRemoteMethod('updateAll', true);
    // User.disableRemoteMethod('updateAttributes', false);
    // User.disableRemoteMethod('find', true);
    // User.disableRemoteMethod('findById', true);
    // User.disableRemoteMethod('findOne', true);
    User.disableRemoteMethod('deleteById', true);
    User.disableRemoteMethod('count', true);
    // User.disableRemoteMethod('exists', true);

};
