module.exports = function (Project) {
    // remove methods from API
    // Project.disableRemoteMethod('create', true);
    // Project.disableRemoteMethod('upsert', true);
    Project.disableRemoteMethod('updateAll', true);
    // Project.disableRemoteMethod('updateAttributes', false);
    // Project.disableRemoteMethod('find', true);
    // Project.disableRemoteMethod('findById', true);
    // Project.disableRemoteMethod('findOne', true);
    // Project.disableRemoteMethod('deleteById', true);
    // Project.disableRemoteMethod('count', true);
    // Project.disableRemoteMethod('exists', true);

    /**
     * Status method
     * @param msg
     * @param cb
     */
    Project.status = function (msg, cb) {
        cb(null, arguments);
    };

    Project.remoteMethod('status', {
        accepts: {arg: 'arg1', type: 'string'},
        returns: {arg: 'res1', type: 'string'},
        http: {verb: 'get'}
    });
};
