var server = require('../server');
var ds = server.dataSources.db;

var models = [
    'project',
    'story',
    'storyUser',
    'organisation',
    'organisationUser',
    'user'
];

// console.log(ds.models.project.definition.properties);
// process.exit(1);

ds.automigrate(models, function (er) {
    if (er) throw er;
    console.log('Loopback tables [' + models + '] created in ', ds.adapter.name);
    ds.disconnect();
});